
//récupérer les liens cliquables 
var links = document.querySelectorAll('.link')
var result = document.getElementById('result')
for ( var i = 0; i < i < links.length; i++ ) {
    var link = links[i]
    link.addEventListener('click', function(e) {
        // initialiser object httpRequest, instancier
        var httpRequest = new XMLHttpRequest()
        // on ne veut pas rediriger l'utilisateur
        e.preventDefault()
        result.innerHTML = 'Chargement ... '
        // récupérer l'état de progression de la requête
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4) {
                result.innerHTML = ''
                if (httpRequest.status === 200) {
                    var results = JSON.parse(httpRequest.responseText)
                    var ul = document.createElement('ul')
                    result.appendChild(ul)
                    for ( var i = 0; i < results.length; i++) {
                        var li = document.createElement('li')
                        li.innerHTML = results[i].name
                        ul.appendChild(li)
                    }
                } else {
                    alert('impossible de contacter le serveur')
                }  
            } 
        }
        // méthode, url, asych (pendant que la requête s'exécute on continue sur lapage)
        httpRequest.open('GET', 'https:/ lder.typicode.com/users', true)
        // on envoie la requête 
        httpRequest.send()
    })
}

/* POST PHP

var result = document.getElementById('result')
var form = document.getElementById('form')

form.addEventListener('submit', function(e) {
    // initialiser object httpRequest, instancier
    var httpRequest = new XMLHttpRequest()
    // on ne veut pas rediriger l'utilisateur
    e.preventDefault()
    result.innerHTML = 'Chargement ... '
    // récupérer l'état de progression de la requête
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            result.innerHTML = ''
            if (httpRequest.status === 200) {
                result.innerHTML = httpRequest.responseText
            } else {
                alert('impossible de contacter le serveur')
            }  
        }
    }
    
    httpRequest.open('POST', '/index.php', true)
    var data = new FormData(form)
    // en ayant passé la variable form dans FormData, plus besoin de lui passer les valeurs une à une
    // var input = document.querySelector('#q')
    // data.append('q', input.value)
    httpRequest.send(data)
    
})

*/

